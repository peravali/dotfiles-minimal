#!/bin/bash

function dotfiles {
   /usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME $@
}

git clone --bare https://gitlab.com/heiko.appel/dotfiles-minimal.git $HOME/.dotfiles

dotfiles checkout 2>/dev/null

if [ $? = 0 ]; then
    echo "Checked out config."
else
    echo "Backing up pre-existing dot files."
    backup_dir=$HOME/.dotfiles-backup-$(date +"%Y-%m-%d")
    mkdir -p ${backup_dir}
    dotfiles checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} ${backup_dir}/{}
fi

dotfiles checkout
dotfiles config status.showUntrackedFiles no
dotfiles submodule update --init --recursive
