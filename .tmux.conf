#-----------------------------------------------------------------------------#
# Status Bar
set-option -g status on                # turn the status bar on
set-option -g status-position top    # position the status bar at top of screen

bind -n S-Pageup copy-mode -u

# force a reload of the config file
unbind r
bind r source-file ~/.tmux.conf

# quick pane cycling
unbind ^A
bind ^A select-pane -t :.+

set -g history-limit 100000

# enable scrolling using the Shift-PageUp/Down keys:
set -g terminal-overrides 'xterm*:smcup@:rmcup@'

bind P paste-buffer

# visual notification of activity in other windows
set -g monitor-activity on
set -g visual-activity on


#-----------------------------------------------------------------------------#
# tmux theme
# https://vagr9k.me/creating-a-native-powerline-theme-for-tmux/
# https://raw.githubusercontent.com/Vagr9K/dotfiles/master/tmux/powerline.tmuxtheme

# Set net speed format
set -g @net_speed_format " %8s  %8s"

# Status bar colors
set -g status-fg colour240
set -g status-bg colour31

# Left status bar
set -g status-left-length 40
set -g status-left "#{?client_prefix,#[fg=colour255]#[bg=colour31]#[bold] #S #[fg=colour31]#[bg=colour233],#[fg=colour232]#[bg=colour255]#[bold] #S #[fg=colour255]#[bg=colour233]}"

# Right status bar
set -g status-right-length 100
set -g status-right "#{net_speed} #[fg=colour236,bg=colour233]#[fg=colour233,bg=colour31] %H:%M:%S #[fg=colour233,bg=colour236]#[fg=colour255,bg=colour233,bold] #{session_attached} #[fg=colour255,bg=colour233]#[fg=colour233,bg=colour255] #(whoami)@#[fg=colour232,bg=colour255,bold]#H "

# Window status
set -g window-status-format "  #I#F #W  "
set -g window-status-current-format "#[fg=colour233,bg=colour31]#[fg=colour232,bg=colour255] #I#F #W #[fg=colour31,bg=colour233,nobold]"

# Window separator
set -g window-status-separator ""

# Window status alignment
set -g status-justify left

# Pane number indicator
set -g display-panes-colour colour233
set -g display-panes-active-colour colour245

# Clock mode
set -g clock-mode-colour colour39
set -g clock-mode-style 24
