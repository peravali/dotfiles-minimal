# Copyright (c) 2021, Heiko Appel
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

#-----------------------------------------------------------------------------#
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

#-----------------------------------------------------------------------------#
# local bash-completion
if [ -d ~/.bash_completion.d ]; then
  for file in ~/.bash_completion.d/*; do
    . $file
  done
fi

#-----------------------------------------------------------------------------#
# local bash functions
if [ -d ~/.bash_local.d ]; then
  for file in ~/.bash_local.d/*; do
    . $file
  done
fi

#-----------------------------------------------------------------------------#
# set the prompt
. $HOME/.prompt

#-----------------------------------------------------------------------------#
# source private bash config (containing credentials, etc)
if [ -f $HOME/.bashrc_private ]; then
. $HOME/.bashrc_private
fi

#-----------------------------------------------------------------------------#
# spack
export SPACK_PYTHON=python3
if [ -f $HOME/spack/share/spack/setup-env.sh ]; then
function spack_setup_env {
 . $HOME/spack/share/spack/setup-env.sh
}
fi

#-----------------------------------------------------------------------------#
# environment variables
export EDITOR=vim
export LS_COLORS="no=00:fi=00:di=00;34:ln=00;36:pi=40;33:so=00;35:bd=40;33;01:cd=40;33;01:or=01;01;37;41:mi=01;01;37;41:ex=00;32:*.cmd=00;32:*.exe=00;32:*.com=00;32:*.btm=00;32:*.bat=00;32:*.m4=00;32:*.sh=00;32:*.csh=00;32:*.tar=00;31:*.tgz=00;31:*.arj=00;31:*.taz=00;31:*.lzh=00;31:*.zip=00;31:*.z=00;31:*.7z=00;31:*.Z=00;31:*.gz=00;31:*.bz2=00;31:*.bz=00;31:*.tz=00;31:*.rpm=00;31:*.cpio=00;31:*.jpg=00;35:*.gif=00;35:*.bmp=00;35:*.xbm=00;35:*.xpm=00;35:*.png=00;35:*.tif=00;35:*.pdf=00;33:*.pdb=00;91:*.ps=00;33:*.ps.gz=00;31:*.eps=00;90:*.deb=00;31:*.F90=00;36:*.f90=00;36:*.f=00;36:*.c=00;36:*.cpp=00;36:*.tex=00;36:*Makefile=00;35:*makefile=00;35:*Makefile.am=00;35:*Makefile.local=00;35:*Makefile.in=00;35:*.nb=00;94:*.conf=00;95:*inp=00;95:*.par=00;92:*.in=00;35:*.m=38;05;28:*.flv=38;05;199:*.avi=38;05;199:*.pdb=38;05;166:*.xyz=38;05;202:*.pbs=00;36:*.tcl=38;05;70:*.hsd=00;35:*.log=38;05;220:*.out=38;05;178:*.gen=38;05;136:*.xml=38;05;129:*.bib=38;05;179:*.asy=38;05;73:*.vmd=38;05;59:*.cube=38;05;140:*.arc=38;05;110:*.aims=38;05;163"

export PATH=/bin:/usr/bin:/usr/sbin/:/usr/bin/X11:/usr/X11R6/bin:/usr/local/bin::$HOME/bin:$HOME/usr/bin:$HOME/devel/bin:.:$PATH

export LESS='-fRMis'
export PAGER='less -r'
export GREP_COLOR="00;31"
export HOSTNAME=$(hostname)
export TERM=xterm-256color
# for pass
export PASSWORD_STORE_CHARACTER_SET="[:alnum:]"
# for caff
export PERL_MAILERS=sendmail:/usr/bin/msmtp

#-----------------------------------------------------------------------------#
# language settings
export LC_ALL=en_US.UTF-8
export LANGUAGE=us
export LANG=us

#-----------------------------------------------------------------------------#
# bash history settings
# adapted from https://unix.stackexchange.com/questions/18212/bash-history-ignoredups-and-erasedups-setting-conflict-with-common-history
# timestamps
HISTTIMEFORMAT=$'\033[32m'"%Y-%m-%d-%H:%M:%S "$'\033[0m'

# extend the history of the shell
HISTSIZE=20000000

HISTCONTROL=ignoredups:erasedups
HISTIGNORE='&:[ ]*:exit'

# append to the history file, don't overwrite it
shopt -s histappend

function historymerge {
    history -n; history -c; history -r;
}
#trap historymerge EXIT
#export PROMPT_COMMAND="history -a; $PROMPT_COMMAND"
#export PROMPT_COMMAND="history -n; history -c; history -r; $PROMPT_COMMAND"
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

#-----------------------------------------------------------------------------#
# shell options

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# correct minor spelling errors
shopt -s cdspell

# Autocorrect on directory names to match a glob.
shopt -s dirspell 2> /dev/null

# regular expressions
shopt -s extglob

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob;

# report the status of terminated background jobs immediately, 
# rather that waiting till the next primary prompt is issued
set -o notify

#-----------------------------------------------------------------------------#
# alias definitions
alias ls='ls --color=auto --time-style=long-iso'
alias l='ls -latr --time-style=long-iso'
alias ll='ls -hal --time-style=long-iso'
alias e='emacs -no-site-file -nw'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias deflate="perl -MCompress::Zlib -e 'undef $/; print uncompress(<>);'"
alias exe='chmod 755'
alias ee='echo -e'
alias xs='xsel -i'
alias cdc='cd $(xclip -o)'
alias pwdc='pwd | xclip'
alias gitc='echo git clone $(git remote get-url origin) | xclip'
alias 7zea='7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on'

# file-creation mask
umask 027

#-----------------------------------------------------------------------------#
# lesspipe
if [ "X$(which lesspipe 2>/dev/null)" != "X" ]; then
  eval "$(lesspipe)"
fi

#-----------------------------------------------------------------------------#
# functions

# command to manage dotfiles
function dotfiles {
   /usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME $@
}

# realpath to xclipboard
p(){
 realpath "${@}" | xclip -r
}

#-----------------------------------------------------------------------------#
# readline X selection
# adapted from https://stackoverflow.com/questions/994563/integrate-readlines-kill-ring-and-the-x11-clipboard
_xdiscard() {
    echo -n "${READLINE_LINE:0:$READLINE_POINT}" | xclip
    READLINE_LINE="${READLINE_LINE:$READLINE_POINT}"
    READLINE_POINT=0
}
_xkill() {
    echo -n "${READLINE_LINE:$READLINE_POINT}" | xclip
    READLINE_LINE="${READLINE_LINE:0:$READLINE_POINT}"
}

_xcopy() {
    #echo -n "${READLINE_LINE:$READLINE_POINT}" | xclip
    echo -n "${READLINE_LINE}" | xclip
}

_xyank() {
    READLINE_LINE="${READLINE_LINE:0:$READLINE_POINT}$(xclip -o)${READLINE_LINE:$READLINE_POINT}"
}

_xclean() {
    xcb -s 0-7 < /dev/null
}

# if this is an interactive shell
if [[ $- = *i* ]]; then
  #bind -m emacs -x '"\eu": _xdiscard'
  #bind -m emacs -x '"\ek": _xkill'
  #bind -m emacs -x '"\ey": _xyank'
  bind -m emacs -x '"\ec": _xcopy'
  bind -m emacs -x '"\ex": _xclean'
fi

#-----------------------------------------------------------------------------#
# environment variables for code development

#export FCFLAGS="-g -Wall -Wno-maybe-uninitialized -march=native -fbacktrace -fcheck=all -fbounds-check -ffree-line-length-none"
#export FCFLAGS="-g -O0 -march=native -fbacktrace -fbounds-check -Wampersand -Wintrinsics-std -Wtabs -Wintrinsic-shadow -Wline-truncation -Wtarget-lifetime -Winteger-division -Wreal-q-constant -Wundefined-do-loop -Wunused-variable -Wzerotrip -Wno-maybe-uninitialized -Wuninitialized -Wcharacter-truncation -Werror -fprofile-arcs -ftest-coverage -fcheck=all,no-array-temps -finit-real=snan -ffpe-trap=invalid,zero -fstack-protector-all"
export FCFLAGS="-g -O0"
# -fsanitize=address -fno-omit-frame-pointer 
# -fstack-protector-all
# -fopenmp
# -ffpe-trap=invalid,zero
# -Wc-binding-type -Wconversion -Wsurprising
# -fallow-argument-mismatch -fallow-invalid-boz

#export MKL_INTERFACE_LAYER=GNU,LP64
#export MKL_THREADING_LAYER=GNU

